<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/1/2017
 * Time: 7:31 PM
 */

    session_start();
    echo $_SESSION['username'];
    $myValue1 = 100;
    $myValue2 = 150;

    function doSomething()
    {
        //echo $myValue;//should be error($myValue) was not initialized;
        $myValue = 50;
        echo $myValue + $GLOBALS['myValue2']. "<br>";//$GLOBALS act as a associative array;
    }
    function doSomething2()
    {
        $myValue = 50;
        echo $myValue + $GLOBALS['myValue1'] + $GLOBALS['myValue2'];
    }

    doSomething();
    doSomething2();

    echo $myValue;

    echo "<pre>";
        echo $_SERVER['PHP_SELF'];
        echo "<br>";
        echo $_SERVER['SERVER_NAME'];
        echo "<br>";
        echo $_SERVER['REQUEST_METHOD'];
        echo $_SERVER['DOCUMENT_ROOT'];
        echo "<br>";
        echo $_SERVER['HTTP_REFERER'];
        echo $_SERVER['REQUEST_TIME'];
        echo "<br>";

    echo "</pre>";

