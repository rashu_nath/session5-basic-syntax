<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/1/2017
 * Time: 9:12 PM
 */
    echo "<pre>";

    $myArr = array(12, "hello", true, 4.3);
    $after_serialized = serialize($myArr);
    print_r($myArr);
    echo "<br>";
    echo $after_serialized;
    echo "<br>";

    $unknownData = unserialize($after_serialized);
    var_dump($unknownData);
    echo "<br>";
    var_export($unknownData); //parseable representaion useable in code itself
    echo "<br>";
    //echo $unknownData;
    echo "<br>";

    echo "</pre>";