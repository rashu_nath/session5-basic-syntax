<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/1/2017
 * Time: 6:27 PM
 */
$myString = "hello world!";
$heredocVariable = <<<BITM
this is me. i am just typing. trustme.
this is me. i am just typing. trustme.
this is me. i am just typing. trustme.
this is me. i am just typing. trustme.
this is me. i am just typing. trustme.
this is me. i am just typing. trustme.
 this is me $myString this is me!
BITM;

echo "$heredocVariable";

$nowdocVariable = <<<'Bitm'

this is me. i am just typing. trustme.
this is me. i am just typing. trustme.
this is me. i am just typing. trustme.
this is me. i am just typing. trustme.
this is me. i am just typing. trustme.
this is me. i am just typing. trustme.
 this is me $myString this is me!

Bitm;

echo "$nowdocVariable";