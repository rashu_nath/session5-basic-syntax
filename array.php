<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/1/2017
 * Time: 6:39 PM
 */

$myArray = array(13, true, 75.25, "me", 5);

echo "<pre>";//used to formating text
    print_r($myArray);
    echo "<br>";
    var_dump($myArray);
//following is example of associative array
    $personAge = array("rahim"=>35, "karim"=>45, "jorina"=>33);//defining index
    echo "<br>";
    print_r($personAge['rahim']."<br>");
    $array1 = (12, "first"=>1, "second"=>2,15);
    echo "<br>";
    print_r($array1);
    echo "<br>";
    $array2 = ([12]=>10, "rahim"=>111, "karim"=>125,5);
    print_r($array2);
    echo "<br>";
echo "<pre>";